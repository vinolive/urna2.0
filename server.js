const express = require('express');
const fs = require('fs');
const app = express();
const porta = 3000;
app.listen(porta);
app.use(express.static(__dirname))
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.get('/', function(req, res){
    res.sendFile(__dirname + 'index.html');
})

app.get('/cargainicial', function(req, res){
    fs.readFile('config.csv','utf8', function (err, data){
        if (err) {
            console.log("Erro ao ler arquivo: " + err);
        } else {
            var candidato = data.split("\n")

        let candidatos = []

        for (let i = 0; i < candidato.length; i++) {
            const element = candidato[i];
            candidatos.push(element.split(","))
        }
        res.send(candidatos)
        }
        
    })
    
})

app.get('/confere', function(req, res){
    fs.readFile('votos.txt','utf8', function (err, data){
        if (err) {
            console.log("Erro ao ler arquivo: " + err);
        } else {
            var candidato = data.split("\n")

        let candidatos = []

        for (let i = 0; i < candidato.length; i++) {
            const element = candidato[i];
            candidatos.push(element.split(","))
        }
        res.send(candidatos)
        }
        
    })
    
})

app.get

app.post('/votar', function (req, res){
    if(req.body.nmRg==undefined||req.body.nmRg==null){
        req.body.nmRg = ""
    }
    let voto = `${req.body.nmRg},${req.body.nmSelect}` 
    msn = {}
    fs.appendFile("votos.txt", `${voto},\n`, function (err) {
        if (err) {
            msn.status=500
            msn.mensagem ="Erro ao registrar voto, contate o administrador do sistema"
            throw err
        } else {
            msn.status=200
            msn.mensagem ="Voto Registrado Com sucesso"
        }
        res.json(msn)
    })
})

// app.post('/votara', function (req, res){
//     let voto = `${req.body.nmSelect}` 
//     var msn = {}
//     fs.appendFile("votos.txt", `${voto},\n`, function (err) {
//         if (err) {
//             msn.status=500
//             msn.mensagem ="Erro ao registrar voto, contate o administrador do sistema"
//             throw err
//         } else {
//             msn.status=200
//             msn.mensagem ="Voto Registrado Com sucesso"
//         }
//         res.json(msn)
//     })
// })